![2015-05-15-144327_1360x768_scrot.png](https://bitbucket.org/repo/7erKg7/images/1114502626-2015-05-15-144327_1360x768_scrot.png)

A friend asked to know more about git, the version control system so I've decided to do a little intro to Git when coding as part of a development team demo thing... it's becoming / has become the de-facto standard for teams managing codebases so it's fun to have a dip into it.
Join if you would like to play with / get to know what git is and how it works etc. I'm not an expert. The demo is creating a simple website as part of the dev team. Only prior knowledge needed is one or more of basic html/css/javascipt
It will take place in Pandon Labs: I've set up a PI as a repository server to which you can 'push' and 'pull' from (like github) all will be explained.
Comment below or PM me if you fancy joining in.